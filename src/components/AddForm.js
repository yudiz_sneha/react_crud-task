import { Form, Button } from "react-bootstrap"
import { UserContext } from '../contexts/UserContext';
import { useContext, useState } from 'react';

const AddForm = () =>{

    const { addUser } = useContext(UserContext);
    const [ newUser, setNewUser ] = useState({
        name: "", email: "", phone: "", address: ""
    });
    const onInputChange = (e) => {
        setNewUser({...newUser,[e.target.name]: e.target.value})
    }
    const { name, email, phone, address } = newUser;
    const handleSubmit = (e) => {
        e.preventDefault();
        addUser( name, email, phone, address );
    }
     return (
        <Form onSubmit={handleSubmit}>
            <Form.Group>
                <Form.Control
                    type="text"
                    placeholder="Name *"
                    name="name"
                    value={name}
                    onChange = { (e) => onInputChange(e)}
                    required
                />
            </Form.Group>
            <Form.Group>
                <Form.Control
                    type="email"
                    placeholder="Email *"
                    name="email"
                    value={email}
                    onChange = { (e) => onInputChange(e)}
                    required
                />
            </Form.Group>
            <Form.Group>
                <Form.Control
                    as="textarea"
                    placeholder="Address"
                    rows={3}
                    name="address"
                    value={address}
                    onChange = { (e) => onInputChange(e)}
                />
            </Form.Group>
            <Form.Group>
                <Form.Control
                    type="text"
                    placeholder="Phone"
                    name="phone"
                    value={phone}
                    onChange = { (e) => onInputChange(e)}
                />
            </Form.Group>
            <Button variant="success" type="submit" block>
                Add New User
            </Button>
        </Form>
     )
}

export default AddForm;