import { Form, Button } from "react-bootstrap"
import { UserContext } from '../contexts/UserContext';
import { useContext, useState } from 'react';

const EditForm = ({ theUser }) =>{

    const id = theUser.id;

    const [ name, setName ] = useState(theUser.name);
    const [ email, setEmail ] = useState(theUser.email);
    const [ address, setAddress ] = useState(theUser.address);
    const [ phone, setPhone ] = useState(theUser.phone);

    const { updateUser } = useContext(UserContext);
    const updatedUser = {id, name, email, address, phone}

    const handleSubmit = (e) => {
        e.preventDefault();
        updateUser(id, updatedUser)
    }

     return (
        <Form onSubmit={handleSubmit}>
            <Form.Group>
                <Form.Control
                    type="text"
                    placeholder="Name *"
                    name="name"
                    value={name}
                    onChange={(e)=> setName(e.target.value)}
                    required
                />
            </Form.Group>
            <Form.Group>
                <Form.Control
                    type="email"
                    placeholder="Email *"
                    name="email"
                    value={email}
                    onChange={(e)=> setEmail(e.target.value)}
                    required
                />
            </Form.Group>
            <Form.Group>
                <Form.Control
                    as="textarea"
                    placeholder="Address"
                    rows={3}
                    name="address"
                    value={address}
                    onChange={(e)=> setAddress(e.target.value)}
                />
            </Form.Group>
            <Form.Group>
                <Form.Control
                    type="text"
                    placeholder="Phone"
                    name="phone"
                    value={phone}
                    onChange={(e)=> setPhone(e.target.value)}
                />
            </Form.Group>
            <Button variant="success" type="submit" block>
                Edit User
            </Button>
        </Form>
     )
}

export default EditForm;